/*
const products = [{
    shampoo: {
        price: "$50",
        quantity: 4
    },
    "Hair-oil": {
        price: "$40",
        quantity: 2,
        sealed: true
    },
    comb: {
        price: "$12",
        quantity: 1
    },
    utensils: [
        {
            spoons: { quantity: 2, price: "$8" }
        }, {
            glasses: { quantity: 1, price: "$70", type: "fragile" }
        },{
            cooker: { quantity: 4, price: "$900" }
        }
    ],
    watch: {
        price: "$800",
        quantity: 1,
        type: "fragile"
    }
}]
*/

// Q1. Find all the items with price more than $65.
function itemsMoreThan$65(data)
{
    let res = Object.entries(data[0]).reduce((acc,item)=>{
        if(!Array.isArray(item[1]))
        {
            if(Number(item[1].price.slice(1))>65)
            acc[item[0]] = item[1];
        }
        else
        {
            let temp = item[1].filter((eachUtensils)=>{
                let key = Object.keys(eachUtensils);
                if(Number(eachUtensils[key].price.slice(1))>65)
                    return true;
            });
            acc.utensils = temp;
        }
        return acc;
    },{});
    return JSON.stringify(res);
}

//console.log(itemsMoreThan$65(products));

// Q2. Find all the items where quantity ordered is more than 1.
function quantityMoreThanOne(data)
{
    let res = Object.fromEntries(Object.entries(data[0]).map(((item)=>{
        if(Array.isArray(item[1]))
        {
            item[1] = item[1].filter(element => {
                let obj = Object.entries(element);
                if(obj[0][1].quantity>1)
                    return true;
            });
        }
        return item;
    })).filter((item)=>{
        
        if(item[1].quantity>1 || Array.isArray(item[1]))
            return true;
        else
            return false;
    }))
    return JSON.stringify(res); 
}

//console.log(quantityMoreThanOne(products));

//Q3. Get all items which are mentioned as fragile.
function fragileItems(data)
{
    let res = Object.entries(data[0]).reduce((acc,item)=>{
        if(item[0] != 'utensils' && item[1].hasOwnProperty('type'))
        {
            if(item[1].type=='fragile')
                acc[item[0]] = item[1];
        }
        if(item[0] == 'utensils')
        {
            let temp = item[1].filter((eachUtensils)=>{
                let key = Object.keys(eachUtensils);
                if(eachUtensils[key].type=='fragile' && eachUtensils[key].hasOwnProperty('type'))
                    return true;
            });
            acc.utensils = temp;
        }
        return acc;
    },{});
    return JSON.stringify(res);
}

//console.log(fragileItems(products));

//Q4. Find the least and the most expensive item for a single quantity.
function leastAndMostExpensive(data)
{
    let res = Object.fromEntries(Object.entries(data[0]).map(((item)=>{
        if(item[0]=='utensils')
        {
            item[1] = item[1].filter(element => {
                let obj = Object.entries(element);
                if(obj[0][1].quantity==1)
                    return true;
            });
        }
        return item;
    })).filter((item)=>{
        
        if(item[1].quantity==1 || item[0]=='utensils')
            return true;
        else
            return false;
    }));
    return JSON.stringify(res);
}
//console.log(leastAndMostExpensive(products));

//Q5. Group items based on their state of matter at room temperature (Solid, Liquid, Gas)

function groupByStates(data)
{
    let res = Object.keys(data[0]).reduce((acc,item)=>{
        if(item=='shampoo' || item=='Hair-oil')
        {
            acc.solid.push({[item]:data[0][item]});
        }
        else
        {
            acc.liquid.push({[item]:data[0][item]});
        }
        return acc;
    },{solid:[], liquid:[], gas:[]});
    
    return JSON.stringify(res);
}

//console.log(groupByStates(products));